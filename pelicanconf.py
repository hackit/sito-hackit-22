#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import json

with open('hackmeeting.json') as buf:
    hm_metadata = json.load(buf)
# da un anno all'altro cambiare solo la variabile YEAR è sufficiente per le operazioni più base!
YEAR = hm_metadata['year']

EDITION = hex(YEAR - 1997)
AUTHOR = "Hackmeeting"
SITENAME = "Hackmeeting %s" % EDITION
CC_LICENSE = "by-nc-sa"
SITEURL = "/hackit%d" % (YEAR - 2000)

PATH = "content"
PAGE_PATHS = ["pages"]
ARTICLE_PATHS = ["news"]
STATIC_PATHS = ["images", "talks", "extra"]
# DIRECT_TEMPLATES = ('search',)  # tipue search

TIMEZONE = "Europe/Paris"

DEFAULT_LANG = "it"

# Feed generation is usually not desired when developing
INDEX_SAVE_AS = "news.html"
ARTICLE_URL = "news/{slug}.html"
ARTICLE_SAVE_AS = "news/{slug}.html"
FEED_DOMAIN = "https://it.hackmeeting.org"
FEED_ALL_ATOM = "news.xml"
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = None
# Social widget
SOCIAL = None
DEFAULT_PAGINATION = 10
USE_OPEN_GRAPH = False  # COL CAZZO

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DEFAULT_DATE = (YEAR, 3, 1)
TYPOGRIFY = True

PAGE_ORDER_BY = "navbar_sort"
PAGE_URL = "{slug}.html"
PAGE_SAVE_AS = "{slug}.html"
PAGE_LANG_URL = "{slug}.{lang}.html"
PAGE_LANG_SAVE_AS = "{slug}.{lang}.html"
BANNER = True
BANNER_ALL_PAGES = True
SITELOGO = "logo/logo.png"
# PAGE_BACKGROUND = 'images/background.jpg'
# THEME = 'themes/hackit0x15/'
THEME = "themes/to0x19/"
FAVICON = "images/cyberrights.png"
FONT_URL = "theme/css/anaheim.css"

# Custom css by sticazzi.
# CUSTOM_CSS = 'theme/css/hackit.css'
EXTRA_PATH_METADATA = {
    # 'extra/main.css': {'path': 'themes/pelican-bootstrap3/static/css/main.css' },
    "extra/favicon.png": {"path": "images/favicon.png"},
    "images/locandina.jpg": {"path": "images/locandina.jpg"},
}

# Pelican bootstrap 3 theme settings
BOOTSTRAP_THEME = "darkly"
HIDE_SITENAME = True
HIDE_SIDEBAR = True
PLUGIN_PATHS = ["plugins"]
PLUGINS = ["langmenu", "talks", "tipue_search", "pelican_webassets"]

# plugin/talks.py
SCHEDULEURL = "https://hackmeeting.org" + SITEURL + "/schedule.html"
TALKS_GRID_STEP = 30

MARKDOWN = {"extension_configs": {"markdown.extensions.toc": {}}}
