Title: Accessibilità
slug: accessibility
navbar_sort: 2

Hackmeeting è uno spazio e una comunità autogestita: tutto quello che 
troverai è autocostruito con povertà di mezzi, generalmente recuperando 
strutture abbandonate e riadeguandole il più possibile ai nostri bisogni.
Abbiamo cercato di rendere Hackmeeting più accessibile che potevamo, ma 
ci sono sicuramente ancora tante cose che non ci sono riuscite e altre 
che dovremmo migliorare. Abbi pazienza e aiutaci segnalandoci cosa non 
va, in modo da fare meglio il prossimo anno!

## Lo spazio

Hackit 2022 si svolgerà al CSOA Gabrio, in Via Francesco Millio 42, Torino.
Lo stabile è una vecchia scuola media occupata, si accede dalla strada 
tramite un cancello largo a doppia anta, l'ingresso ha dei gradini ma sarà presente
un ingresso alternativo senza barriere architettoniche.
Gli spazi saranno segnalati con cartelli in lingua scritta e disegnata.
Nel quartiere non è semplicissimo trovare parcheggio, ma lo spazio di 
fronte all'ingresso sarà sempre tenuto libero.

## Aule seminari

Le aule dei seminari saranno 3:
- una grande sala concerti, situata al piano terra. Uno spazio molto 
largo dove le persone possono mantenere distanza.
- le altre due saranno all'aperto in giardino, coperte da tensostrutture.

Il venerdì sera i concerti live si svolgeranno al piano inferiore. Si 
accede tramite scale, non è presente ascensore o rampa. Se hai bisogno 
di scendere rivolgiti all'info point all'ingresso e ti daremo supporto.

In caso di pioggia le due aule previste all'esterno saranno riallestite 
all'interno in spazi al piano terra o accessibili tramite rampa.

## Cucina

La cucina e lo spazio per mangiare si trovano al primo piano. Si accede 
tramite scale oppure anche con una larga rampa che dal giardino porta al 
piano.

## Dormire

Lo spazio per dormire è purtroppo limitato: verrà allestito un piccolo 
spazio tende in giardino e un dormitorio comune nella palestra. Non ci 
sono letti a disposizione o stanze singole.

## Bagni

I bagni del Gabrio sono 4 al piano terra e 1 al primo piano 
(quest'ultimo accessibile tramite rampa). Oltre a questi, all'esterno 
c'è un bagno chimico singolo.
I bagni più confortevoli per le persone con ridotta mobilità sono quello 
al primo piano e il chimico in giardino. I bagni al piano terra sono 
meno attrezzati e comodi, ma comunque accessibili.
Tutti i bagni sono gender neutral.
Le docce verranno costruite nei giorni precedenti Hackmeeting: saranno 
spartane e difficilmente consentiranno l'utilizzo a persone con ridotta 
mobilità.

## Modalità aereo

Hackmeeting a volte può essere faticoso: se hai bisogno di prenderti un 
attimo di pausa abbiamo allestito una stanza dove puoi startene per i 
fatti tuoi senza che i tuoi spazi vitali vengano disturbati. Sarà al 
primo piano, a cui si accede tramite scale o rampa.

## Clima

All'interno del Gabrio non avremo aria condizionata, gli spazi 
all'esterno saranno ombreggiati.

## Linguaggi

Non siamo ancora in grado di garantire un servizio di traduzioni in 
simultanea, ma Hackmeeting è frequentato da persone che parlano diverse 
lingue, compresa la LIS. Se hai bisogno, o se vuoi offrirti per 
tradurre, rivolgiti all'info point.

## Cani da assistenza

Se hai un cane che ti accompagna è il benvenuto. Troverà acqua e 
probabilmente altri cani con cui giocare. Se quest'ultima cosa dovesse 
essere un problema segnalacelo all'info point all'ingresso e cercheremo 
di limitare l'eventuale invadenza degli altri cani.

## Altre info

Nello spazio saranno presenti un po' ovunque prese per ricaricare 
qualunque dispositivo.
La musica ad alto volume sarà presente in modo continuativo solo venerdì 
sera e in un luogo lontano dagli altri spazi comuni.
Per qualsiasi altra informazione o necessità che non stiamo 
considerando, puoi rivolgerti all'info point all'ingresso.

Torna alla pagina delle [informazioni principali]({filename}info.md)
