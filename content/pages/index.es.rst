About
###################

:slug: index
:navbar_sort: 1
:lang: es

9-11/09/2022  - Torino
========================================

Hackmeeting es el encuentro anual de las contraculturas digitales italianas, de aquellas comunidades que analizan de manera crítica los mecanismos de desarollo de las tecnologías en nuestra sociedad. Pero hackmeeting no es sólo esto, es mucho más. Te lo contamos al oído, no se lo digas a nadie, el hackmeeting es solamente para verdaderos hackers, para quienes quieran gestionarse la vida como quieran y luchan por eso, aunque no hayan visto un ordenador en su vida.

Tres días de charlas, juegos, fiestas, debates, intercambios de ideas y aprendizaje colectivo, para analizar juntxs las tecnologías que usamos todos los días, cómo cambian y cómo pueden impactar en nuestras vidas, tanto reales como virtuales. Un encuentro para indagar qué papel podemos jugar en este cambio y liberarnos del control de aquellos que quieren monopolizar su desarrollo, rompiendo nuestras estructuras sociales y relegándonos a espacios virtuales cada vez más limitados.

**El evento es totalmente autogestionado: no hay ni organizador@s ni asistentes, solamente participantes!**
