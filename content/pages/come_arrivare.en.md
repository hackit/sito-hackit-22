Title: How to get there
slug: come-arrivare
lang: en

Hackmeeting 2022 will be held at [CSOA Gabrio](https://gabrio.noblogs.org/) - [Via Francesco Millio 42, Torino](https://www.openstreetmap.org/?mlat=45.05900&mlon=7.64895#map=17/45.05900/7.64895)

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=7.639650106430055%2C45.05553070985391%2C7.657567262649537%2C45.06244998798831&amp;layer=mapnik&amp;marker=45.05899045358653%2C7.648608684539795" style="border: 1px solid black"></iframe><small><a href="https://www.openstreetmap.org/?mlat=45.05899&amp;mlon=7.64861#map=17/45.05899/7.64861" target='_blank'>View map</a></small>

## With public transport

#### From Porta Susa (Closer than Porta Nuova)

take the bus 55 and get down at the stop ROBILANT SUD

#### From Porta Nuova

Take the bus 58B direction SALVEMINI for 9 stops get down at stop 1940 – SPALATO (V.BOBBIO / V.SPALATO)


## From Caselle Airport 

#### By bus - ticket is 7.5-8.5€  
Take the bus sadem 268 and get down in Porta Susa  
At Porta Susa take the bus 55 and get down at the stop ROBILANT SUD

#### By train and bus - ticket is ~4€  
Get to the train station next to the airport and take one of the trains SFMA in direction Torino (there is one almost every 1h 1h30mn)
You will have to get own in Venaria because of maintenance work on the railline
and take either the direct bus SF2 or the Bus 11 and get down to Porta Susa  
At Porta Susa take the bus 55 and get down at the stop ROBILANT SUD


## By car

You can find parking here:

[Via Gabriele D'Annunzio, Polo Nord, Circoscrizione 3, Turin, Torino, Piedmont, 10129, Italy](https://www.openstreetmap.org/way/106366574)

or

[Piazzale Gian Maria Volontè, Borgo San Paolo, Circoscrizione 3, Turin, Torino, Piedmont, 10129, Italy](https://www.openstreetmap.org/way/83808962)




