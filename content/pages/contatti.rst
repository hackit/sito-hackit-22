Contatti
###########

:slug: contact
:navbar_sort: 6
:lang: it

**Mailing List**

La comunità Hackmeeting ha una `lista di discussione <https://www.autistici.org/mailman/listinfo/hackmeeting>`_ dove poter chiedere informazioni e seguire le attività della comunità. La lista ha un `archivio pubblico <http://lists.autistici.org/list/hackmeeting.html>`_, quindi puoi leggerla anche senza iscriverti. L'iscrizione è invece necessaria per scrivere.

**IRC**

Esiste anche un canale IRC (Internet Relay Chat) dove poter discutere e chiacchierare con tutti i membri della comunità: collegati al server ``irc.autistici.org`` ed entra nel canale ``#hackit99``.

Se preferisci XMPP/Jabber, puoi raggiungere lo stesso canale come ``#hackit99@mufhd0.esiliati.org`` (includi il cancelletto).

**Mail**

Per questioni logistiche, scrivi a: infohackit@autistici.org. In particolare per l'ospitalità a dormire visto
che i posti non saranno tantissimi.


Sito
---------

Se vuoi modificare il sito, vedi le info su https://0xacab.org/hackit/sito-hackit-22#user-content-hackmeeting-2022
