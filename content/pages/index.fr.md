Title: About
Date: 2016-04-17
Slug: index
navbar_sort: 1
lang: fr

9-11/09/2022  - Torino
========================================

*Hackmeeting* est la rencontre annuel des cultures numériques alternatives
italiennes, des communautés qui agissent de façon critique face aux mécanismes
de développement des technologies dans notre société. Mais c'est pas seulement
ça: on y trouve bien plus. On te le chuchote à l'oreille, ne le dis à personne:
hackit est seulement pour les vrais hackers, c'est à dire pour ceux/celles qui
veulent conduire leur vie comme ils/elles préfèrent, et qui savent comment se
battre pour accomplir leur objectif; même s'ils/elles n'ont jamais vu un ordi.


Trois jours entre talk techniques, jeux, fêtes, débats, discussions et
apprentissage collectif, tout ça pour étudier tous ensemble les technologies
qu'on utilise tous les jours, leur développement et les changements qu'elles
provoquent dans le réel et le virtuel des nos vies; pour comprendre quel soit
le rôle qu'on puisse jouer pour adresser ces changement vers la libération à
las fois des technologies elles-mêmes et des nos vies.

**L'événement est complètement autogéré: il n'y a que des participants, pas
d'organisateurs, pas d'entrepreneurs.**
