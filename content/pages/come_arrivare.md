Title: Come arrivare
slug: come-arrivare
navbar_sort: 2
lang: it

Hackit 2022 si svolgerà al [CSOA Gabrio](https://gabrio.noblogs.org/), in [Via Francesco Millio 42, Torino](https://www.openstreetmap.org/?mlat=45.05900&mlon=7.64895#map=17/45.05900/7.64895)

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=7.639650106430055%2C45.05553070985391%2C7.657567262649537%2C45.06244998798831&amp;layer=mapnik&amp;marker=45.05899045358653%2C7.648608684539795" style="border: 1px solid black"></iframe><small><a href="https://www.openstreetmap.org/?mlat=45.05899&amp;mlon=7.64861#map=17/45.05899/7.64861">Visualizza mappa ingrandita</a></small>



## Coi mezzi pubblici
Porta Susa è più vicina di Porta Nuova

#### Da Porta Susa

prendere il 55 e scendere alla fermata ROBBILANT SUD

#### Da Porta Nuova

Prendere la linea 58B in direzione Salvemini per 9 fermate.  
Scendere alla fermata 1940 – SPALATO (V.BOBBIO / V.SPALATO)


## In auto
Se arrivi in auto difficilmente troverai parcheggio vicinissimo, puoi provare qui e venire a piedi:   
[Via Gabriele D'Annunzio, Polo Nord, Circoscrizione 3, Turin, Torino, Piedmont, 10129, Italy](https://www.openstreetmap.org/way/106366574)

oppure qui:

[Piazzale Gian Maria Volontè, Borgo San Paolo, Circoscrizione 3, Turin, Torino, Piedmont, 10129, Italy](https://www.openstreetmap.org/way/83808962)


## Dall'Aeroporto di Caselle

#### In bus diretto (45mn) 7.5-8.5€ di biglietto.

Prendere il bus 268 direzione Torino e scendi a Porta Susa
A porta Susa prendere il 55 e scendere alla fermata ROBILANT SUD


#### In treno + bus - 4€ di biglietto

Dalla stazione accanto all aeroporto prendi il treno SFMA direzione TORINO
Scendere a Venaria (per causa lavori) e prendere il bus diretto SF2 o il bus 11 e scendere a Porta Susa
A porta Susa prendere il 55 e scendere alla fermata ROBILANT SUD


