Title: Info
slug: info
navbar_sort: 1

#### Dove

<abbr title="Centro Sociale Occupato Autogestito">CSOA</abbr> Gabrio, Via Millio 42, Torino, Italia

[Qui trovi tutte le informazioni su come arrivare]({filename}/pages/come_arrivare.md)

#### Quando

Da venerdì 9 a domenica 11 settembre 2022

#### Dormire

L'ospitalità per dormire al Gabrio è prevista da giovedì 8 a domenica 11 settembre. 

Verrà allestito uno spazio tende in giardino e un dormitorio comune nella palestra.

I posti sono limitati quindi chi avesse la possibilità è incoraggiato a trovare sistemazioni alternative per dormire a Torino.  
È **molto importante** in ogni caso comunicarci la tua presenza usando [questo pad](https://pad.cisti.org/p/hackit_presenze_a_dormire).

Non ci sono letti nè stanze singole e non c'è spazio per camper o furgoni.

<!-- 
Per quest'anno, vista la scarsità di posti letto all'interno di Hackmeeting,
chi ne avesse la possibilità è incoraggiato a trovare sistemazioni alternative
per dormire a Torino.

Lo spazio per dormire è purtroppo limitato: verrà allestito un piccolo spazio
tende in giardino e un dormitorio comune nella palestra. Non ci sono letti a
disposizione o stanze singole, non c'è spazio per camper o furgoni. -->

Se hai dei dubbi scrivici a [infohackit@autistici.org](mailto:infohackit@autistici.org)

#### Mangiare

Saranno garantiti colazioni/pranzi/cene a un prezzo popolare.

La cucina sarà vegana, se avete delle necessità particolari (allergie o altro) segnalatecelo.

#### Lavarsi

Ci saranno due docce esterne, nella zona tende, ma saranno spartane e difficilmente consentiranno l’utilizzo a 
persone con ridotta mobilità.

#### Hackmeeting è accessibile?

Speriamo di sì! Trovi tutte le informazioni nella [pagina dedicata
all'accessibilità]({filename}accessibilita.md)

#### Chi organizza l'hackmeeting?

L’hackmeeting è un momento annuale di incontro di una comunità che si riunisce
intorno a [una mailing list](https://www.autistici.org/mailman/listinfo/hackmeeting). 
Non esistono chi organizza e chi fruisce. Tutte le persone possono iscriversi e partecipare
all’organizzazione dell'evento, semplicemente visitando il sito [hackmeeting.org](https://hackmeeting.org) ed entrando nella comunità.

#### Chi è un@ hacker?

Le persone cosiddette hacker sono persone curiose, che non accettano di non poter mettere le mani
sulle cose. Che si tratti di tecnologia o meno chi è hackers reclama la libertà
di sperimentare. Smontare tutto, e per poi rifarlo o semplicemente capire come
funziona. Sono persone che risolvono problemi e costruiscono le cose, credono nella
libertà e nella condivisione. Non amano i sistemi chiusi. La forma mentis
di chi è hacker non è ristretta all’ambito del software-hacking: ci sono persone
che mantengono un attitudine all'hacking in ogni campo dell’esistente, sono persone curiose spinte
da un istinto creativo.

#### Chi tiene i seminari?

Chi ne ha voglia. Se qualche persona vuole proporre un seminario, non deve fare altro
che proporlo in lista. Se la proposta piace, si calendarizza. Se non piace, si
danno utili consigli per farla piacere.

#### Ma cosa si fa, a parte seguire i seminari?

Esiste un “LAN-space”, vale a dire un’area dedicata alla rete: ogni persona arriva
col proprio portatile e si può mettere in rete con altre persone. In genere in
questa zona è facile conoscere altre creature curiose, magari disponibili per farsi aiutare a
installare Linux, per risolvere un dubbio, o anche solo per scambiare quattro
chiacchiere. L’hackmeeting è un open-air festival, un meeting, un hacking
party, un momento di riflessione, un’occasione di apprendimento collettivo, un
atto di ribellione, uno scambio di idee, esperienze, sogni, utopie.

#### Quanto costa l’ingresso?

Come ogni anno, l’ingresso all’Hackmeeting è del tutto libero; ricordati però
che organizzare l’Hackmeeting ha un costo. Le spese sono sostenute grazie ai
contributi volontari, alla vendita di magliette e altri gadget e in alcuni casi
all’introito del bar e della cucina.

#### Cosa devo portare?

Se hai intenzione di utilizzare un computer, portalo accompagnato da una
ciabatta elettrica. Non dimenticare una periferica di rete di qualche tipo
(cavi ethernet, switch, access point). Ricordati inoltre di portare tutto
l’hardware su cui vorrai smanettare con gli altri. Durante l’evento la
connessione internet sarà limitata quindi, se vuoi avere la certezza
organizzati portando il necessario per te e per chi ne avra' bisogno.
In generale ogni persona organizza e partecipa cercando di essere autosufficiente
sul lato tecnologico portandosi l'hardware o costruendoselo al hackmeeting!

#### Posso arrivare prima di venerdì?

Vuoi arrivare qualche giorno prima? Fantastico! Nei giorni precedenti ad hackmeeting ci sono sempre molte cose
da fare (preparare l’infrastruttura di rete, preparare le sale seminari, e tanto altro!) quindi una mano è
assolutamente ben accetta. Molte persone arrivano prima, quindi non sarai sola, anzi!

L'ospitalità al Gabrio è prevista da giovedì e purtroppo per i giorni
precedenti non abbiamo più spazi dove collocarti ma, se l'ospitalità non ti è
necessaria sei assolutamente benvenuta! 

Se hai dei dubbi scrivici a [infohackit@autistici.org](mailto:infohackit@autistici.org)

#### Posso scattare foto, girare video, postare, taggare, condividere, uploadare?

Pensiamo che ad ogni partecipante debba essere garantita la libertà di
scegliere in autonomia l’ampiezza della propria sfera privata e dei propri
profili pubblici; per questo all’interno di hackmeeting sono ammessi fotografie
o video SOLO SE CHIARAMENTE SEGNALATI E PRECEDENTEMENTE AUTORIZZATI da tutte e
tutti quanti vi compaiano.

Le persone che attraversano hackmeeting hanno particolarmente a cuore il concetto di privacy: prima di fare
una foto, esplicitalo!

#### Come ci si aspetta che si comportino tutte e tutti?

Hackmeeting è uno spazio autogestito, una zona temporaneamente autonoma e chi
ci transita è responsabile che le giornate di hackit si svolgano nel rispetto
dell’antisessismo, antirazzismo e antifascimo. Se subisci o assisti a episodi
di oppressione, aggressione, brute force, port scan, ping flood e altri DoS non
consensuali e non sai come reagire o mitigare l’attacco, conta sul sostegno di
tutta la comunità e non esitare a richiamare pubblicamente l’attenzione e
chiedere aiuto.


