Title: Call for contents
slug: call
navbar_sort: 4
lang: en


Hackmeeting is the yearly meeting of the Italian countercultures related to
hacking, communities which have a critical relationship with technology.

This year it will be held in [Turin at C.S.O.A. Gabrio]({filename}/pages/info.en.md) from Friday, September 9 to Sunday, September 11, 2022.

Our idea of hacking touches every aspect of our lives and it doesn't stop at
the digital realm: we believe in the use of knowledge and technology to
understand, modify and re-create what is around us.

Unfortunately reality is hard to read, hard to modify and interacting with it
is a hard task.

We want to take control.

It's not a vague idea, generic guidelines or aspiration, it's a pragmatic
organization effort based on solidarity, complicity and sharing of knowledge,
methods and tools to take back everything and take it back together, one piece
at a time.

If you think you can enrich the event with a contribution, send your proposal
joining the [hackmeeting mailing list](https://www.autistici.org/mailman/listinfo/hackmeeting) or sending a message to
[infohackit@autistici.org](mailto:infohackit@autistici.org) with subject `[talk] title` or `[lab]
lab_title` and this info:

* Length (multiple of 30 minutes, max 2 hours)
* (optional) day/time of the day requirements
* Abstract, optional links and references
* Nickname
* Language
* Projector
* Can we record you? (audio only)
* Anything else you might need

#### More concretely

We will set up three outdoor locations (indoor in case of
rain), with microphone/speakers and projector.
If you think your presentation will not need this much time, you can propose
something directly at hackmeeting: a `ten minute talk` of a maximum of 10
minutes.
These talks will be held in the largest available space at the end of of the
day on Saturday. There will be someone who will warn you if you are about to
exceed the allocated time.

If you want to share your discoveries or curiosities in an even more informal
and chaotic way, you will be able to to get a place with your hardware on the
shared tables in the `LAN space`. You will find morbid curiosity, power and
wired connection (bring a power strip, network cables and whatever you might
find useful).

Have any questions or you are shy? Write us at [infohackit@autistici.org](mailto:infohackit@autistici.org)

Would you like to talk about something that hasn't yet been brought up? Add
it, along with your nick, to [this pad](https://pad.cisti.org/p/hackit-desiderata) or send a mail to the
hackmeeting list and hope that somebody else wants to share their knowledge
and steps up.
