Title: Propaganda
slug: propaganda
lang: it
navbar_sort: 10




#### Logo Hackmeeting


<img src='{static}/images/LogoHM22_magenta.svg' width='350px' />  
[download svg magenta]({static}/images/LogoHM22_magenta.svg) - 
[download svg white]({static}/images/LogoHM22.svg) -
[download png]({static}/images/LogoHM22.png) - 
[download pdf]({static}/images/LogoHM22.pdf)


#### Locandine

<img alt='locandina web' src='{static}/images/locandina_web.jpg' width='400px'/>

[download pdf da stampare]({static}/images/locandina_stampa.pdf) -
[download jpg]({static}/images/locandina_web.jpg)

##### Sticker & postcard

[download sticker pdf]({static}/images/stickerHM_date.pdf) -
[download postcard pdf]({static}/images/cartolinaHM.pdf)

### Spot radio


Ecco alcuni spot che è possibile mandare in radio:

##### Forum version

<audio controls >
    <source src="{static}/images/hackit-forum.ogg" type="audio/ogg" />
</audio> [download]({static}/images/hackit-forum.ogg)


##### Tearz version

<audio controls >
    <source src="{static}/images/hackit-tearz.ogg" type="audio/ogg" />
</audio> [download]({static}/images/hackit-tearz.ogg)


##### Conquista di mondo version

<audio controls >
    <source src="{static}/images/hackit-conquista-mondo.ogg" type="audio/ogg" />
</audio> [download]({static}/images/hackit-conquista-mondo.ogg)



<!--

In questa pagina trovate alcuni materiali utili per diffondere il verbo di hackmeeting in ogni dove.

## Grafiche

### Logo

[Logo (nero su trasparente)]({static}/images/logo-hm-trasp.png)

[Grafica maya completa (nero su bianco)](https://hackmeeting.org/media/hackit20/grafica-maya-nerosubianco.jpg)

[Grafica maya completa (bianco su nero)](https://hackmeeting.org/media/hackit20/grafica-maya-biancosunero.jpg)

### Locandina

[Versione stampabile A3 in PDF](https://hackmeeting.org/media/hackit20/hm2020-locandina-print.pdf)

### Flier

[Flier in PDF](https://hackmeeting.org/media/hackit20/flier-print.pdf) vanno stampati fronte retro e poi
tagliati "a croce" in modo da ottenere 4 volantini A6 fronte retro per ogni foglio.

## Spot radio

Ecco alcuni spot che è possibile mandare in radio:

### Fratellino beriversion

<audio controls >
<source
src="https://archive.org/download/spot-hackit20-fratellino/spot-hackit20-fratellino-beriedition-compressed.ogg" type="audio/ogg" />
<source
src="https://archive.org/download/spot-hackit20-fratellino/spot-hackit20-fratellino-beriedition-compressed.mp3"
type="audio/mpeg" />
</audio> -->


<!-- ### Fratellino franceversion

<audio controls >
<source
src="https://archive.org/download/spot-hackit20-fratellino/spot-hackit20-fratellino-franceedition-compressed.ogg"
type="audio/ogg" />
<source
src="https://archive.org/download/spot-hackit20-fratellino/spot-hackit20-fratellino-franceedition-compressed.mp3"
type="audio/mpeg" />
</audio>

 -->


