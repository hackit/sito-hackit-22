About
#####

:navbar_sort: 1
:lang: en
:slug: index

9-11 September / Turin
========================================

 Hackmeeting is the yearly Italian digital counter-cultures meeting; it gathers those communities that take a hard look at how technologies work in our society. And that's not all. We tell you, just you, in a whisper (don't even tell anybody!): Hack-it is just for real hackers, that is to say for those people who want to manage their own lives as they want and are ready to fight for this right, even though they haven't ever seen a computer in their life.

 Three days of lessons, games, parties, debates, crossfires and collective learning, analyzing together those technologies that we use everyday, the way they change and how they can impact on our real or virtual lives; which role we can play in order to redirect these changes and set us free of control from those who want to monopolize their development, letting society crumble and relegating us in even tighter virtual spaces.

 **The event is totally self-managed: there are neither promoters nor users, just participants.**

