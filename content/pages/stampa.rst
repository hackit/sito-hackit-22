Stampa
#########

:slug: press
:navbar_sort: 9
:lang: it

Cartella stampa
""""""""""""""""""""""""""""""""""""""


Propaganda
"""""""""""""""""""""""""""""""""""""""

I materiali utili per la diffusione di hackmeeting 2022 sono nell'apposita pagina `Propaganda
<{filename}propaganda.md>`_


Foto
"""""""""""""""""""""""""""""""""""""""


.. image:: images/press/2016-IMG_0578.jpg
    :height: 200px
    :alt: La bacheca dei seminari dell'hackmeeting 2016, a Pisa
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0578.jpg

.. image:: images/press/2016-IMG_0581.jpg
    :height: 200px
    :alt: Sessione di elettronica digitale
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0581.jpg

.. image:: images/press/2016-IMG_0584.jpg
    :height: 200px
    :alt: Programmazione
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0584.jpg

.. image:: images/press/2016-IMG_0586.jpg
    :height: 200px
    :alt: Computer
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0586.jpg

.. image:: images/press/2016-IMG_0589.jpg
    :height: 200px
    :alt: Il LAN party: un posto dove sperimentare insieme
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0589.jpg

.. image:: images/press/2016-IMG_0574.jpg
    :height: 200px
    :alt: Un hack su Emilio: il famoso robottino degli anni '90 è stato riprogrammato
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0574.jpg


Comunicati stampa
"""""""""""""""""""""""""""""""""""""""




Comunicato Stampa - 6 Settembre
=================================

| HACKMEETING
| 9-11 settembre 2022
| Centro Sociale Occupato Autogestito Gabrio
| Via Millio 42, TORINO
|


Tre giorni di seminari, giochi, dibattiti, scambi di idee e
apprendimento collettivo. Torna l'hackmeeting, l'annuale incontro di chi
si definisce hacker in Italia e che quest'anno si svolge al centro
sociale Gabrio di Torino, dal 9 all'11 settembre. Un momento di
condivisione della conoscenza. Non per inseguire il sogno della prossima
startup destinata a diventare una macchina da soldi, ma per analizzare
assieme le tecnologie che utilizziamo quotidianamente: come cambiano e
come incidono sulle nostre vite reali e virtuali e quale ruolo possiamo
rivestire nell’indirizzare questo cambiamento per liberarlo dal
controllo di chi vuole monopolizzarne lo sviluppo, sgretolando i tessuti
sociali e relegandoci in spazi virtuali sempre più stretti.

Tanti i seminari, da quelli dedicati ai software per aggirare la
censura, a quelli per l'organizzazione dei collettivi di attivisti, alle
tecnologie per lo smartphone dedicate alle persone sorde o ipovedenti. E
ancora: la sessualità e il suo rapporto con la rete, l'installazione di
Linux su computer e telefonini, le insidie dell'intelligenza artificiale
(con il rischio di consolidare pratiche discriminatorie dietro
l'apparente oggettività delle macchine), le strategie della propaganda
sui social, il futuro del software libero.

Seminari aperti a tutte le persone e gratuiti, per mettere insieme idee
e competenze. Una formula ormai molto consolidata, giunta alla 25esima
edizione (sul manifesto indicato in notazione esadecimale: 0x19). La
prima volta fu a Firenze. Era il 1998. In questo quarto di secolo
l'hackmeeting ha accompagnato tutte le fasi della rete. Lanciando
l'allarme con anni di anticipo su quello che sarebbe successo in Italia
e nel mondo. La voracità di Google sui dati delle persone, la
pervasività dei social e le dipendenze che hanno provocato, il controllo
sociale attraverso i dispositivi, le bolle digitali fatte di propaganda
personalizzata, la trasformazione delle criptovalute da strumenti di
libertà in mezzi speculativi, la fine della neutralità della rete, le
insidie del telefonino nei rapporti di lavoro o come strumento di
sorveglianza. L’hackmeeting è un momento annuale di incontro per
osservare, criticare e discutere di queste e altre tematiche, indicando
possibili strade alternative. E continua a esserlo, con questa nuova
edizione.


