Warmup
======

:slug: warmup
:navbar_sort: 5
:lang: it

Cosa sono
"""""""""""""""""""""""""""""""""""""""

I warmup sono eventi "preparatori" ad hackmeeting. Avvengono in giro per l'Italia, e possono trattare gli argomenti più disparati.

Proporre un warmup
"""""""""""""""""""""""""""""""""""""""

Vuoi fare un warmup? ottimo!

* iscriviti alla `mailing list di hackmeeting <https://www.autistici.org/mailman/listinfo/hackmeeting>`_.
* scrivi in mailing list riguardo al tuo warmup: non c'è bisogno di alcuna "approvazione ufficiale", ma segnalarlo in lista è comunque un passaggio utile per favorire dibattito e comunicazione.


Elenco
"""""""""""""""""""""""""""""""""""""""
.. contents:: :local:

==========================

hack or di(y|e)
_________________

4-5 novembre 2022 `@Vag61 <https://vag61.noblogs.org>`_, Bologna
`Hack or di(y|e) <https://hacklabbo.indivia.net/hackordiye22/>`_


Hackrocchio 
___________
9-10 aprile 2022 `@Edera Squat <https://ederasquat.noblogs.org>`_, Torino
`HackЯocchio <https://hackrocchio.org>`_



Presenzatione Gli Asini Rivista dedicato al DIGITALE
____________________________________________________
30 Giugno h18:00, `@Archivio dei movimenti di via Avesella <http://archivioavesella.org/>`_, Bologna.
Presentazione del numero di Maggio 2022 di Gli Asini Rivista dedicato al DIGITALE insieme alla redazione, alle autrici ed ad Hacklabbo: https://gliasinirivista.org/rivista/maggio-99-2022/

.. image:: images/30giugno-asini_bis.png
   :height: 200px
   :alt: Locandina "presentazione 99 Asini rivista"
   :name: Locandina "presentazione 99 Asini rivista"
   :target: images/30giugno-asini_bis.png



ELECTRIC DREAMS 
_______________
da Venerdi 29 Luglio a Domenica 31 Luglio 2022 
@laboratorio e deposito musei MIAI e MusIF Edificio Ex-CUD – Via C.B. Cavour, Rende (CS)  

`ELECTRIC DREAMS II EDIZIONE <https://electricdreams.noblogs.org/>`_



