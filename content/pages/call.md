Title: Call for contents
slug: call
navbar_sort: 4
lang: it

Hackmeeting è l'incontro annuale delle controculture italiane legate
all'hacking, comunità che vivono in maniera critica il rapporto con la
tecnologia.

Quest'anno si terrà a [Torino al C.S.O.A. Gabrio]({filename}/pages/info.md) dal venerdì 9 a domenica 11 settembre 2022.

La nostra idea di hacking investe ogni ambito della nostra vita e non si
limita al digitale: crediamo nell'uso del sapere e della tecnologia per
conoscere, modificare e ricreare quanto ci sta attorno.

Purtroppo la realtà è difficile da leggere, complicata da modificare e
interagire con essa è impegnativo.

Vogliamo riprenderne il controllo.

Non è un idea vaga, una generica linea guida o aspirazione, è invece una
pragmatica capacità organizzativa basata sulla solidarietà, complicità e la
messa in comune di conoscenze, metodi e strumenti per riprenderci tutto e
riprendercelo insieme, un tassello per volta.

In sintesi se pensi di poter arricchire l'evento con un tuo contributo manda
la tua proposta iscrivendoti alla [mailing list di hackmeeting](https://www.autistici.org/mailman/listinfo/hackmeeting) mandando una mail con oggetto `[talk]
titolo_del_talk` oppure `[laboratorio] titolo_del_lab` e queste informazioni:

* Durata (un multiplo di 30 minuti, massimo due ore)
* Eventuali esigenze di giorno/ora
* Breve Spiegazione, eventuali link e riferimenti utili
* Nickname
* Lingua
* Necessità di proiettore
* Disponibilità a farti registrare (solo audio)
* Altre necessità

#### Nel concreto

Allestiremo tre spazi all'aperto, o al coperto in caso di pioggia, muniti di
amplificazione e proiettore.
Se pensi che la tua presentazione non abbia bisogno di tempi cosi lunghi, puoi
proporre direttamente ad hackmeeting un `ten minute talk` di massimo 10
minuti.
Questi talk verranno tenuti nello spazio più capiente al termine della
giornata di sabato; ci sarà una persona che ti avviserà quando stai per
eccedere il tempo massimo.

Se invece vuoi condividere le tue scoperte e curiosità in modo ancora piu
informale e caotico, potrai sistemarti con i tuoi ciappini, sverzillatori,
ammennicoli ed altre carabattole sui tavoli collettivi del `LAN space`.
Troverai curiosità morbosa, corrente alternata e rete via cavo (portati una
presa multipla, del cavo di rete e quel che vuoi trovare).

Hai una domanda estemporanea o una timidezza? Scrivici a
[infohackit@autistici.org](mailto:infohackit@autistici.org)

Ti piacerebbe che si parlasse di un argomento che non è stato ancora proposto?
Aggiungilo insieme al tuo nick in [questo pad](https://pad.cisti.org/p/hackit-desiderata) o manda una mail in lista
hackmeeting e spera che qualcun* abbia voglia di condividere le sue
conoscenze e si faccia avanti.
